//
// Using Boost program options
//   style:   ./options [options] <required arg>
//   example: ./options --x=3 filename
//

#include <iostream>
#include <string>
#include <valarray>
#include <cmath>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <boost/program_options.hpp>


#include "power.h"
#include "cosmology.h"

using namespace std;
using namespace boost::program_options;

static double f, k_min, dk;
static int nmu;

void write_power2mu(const int nc, const double boxsize,
		    PowerSpectrum const * const ps);

int main(int argc, char* argv[])
{
  //
  // command-line options (Boost program_options)
  //
  options_description opt("options [options] filename");
  opt.add_options()
    ("help,h", "display this help")
    ("filename,f", value<string>(), "filename")
    ("nc", value<int>()->default_value(64), "number of grids per dimension")
    ("boxsize", value<double>()->default_value(1000.0), "boxsize")
    ("z", value<double>()->default_value(0.0), "redshift")
    ("omega-m", value<double>()->default_value(0.308), "Omega matter")
    ("k-min", value<double>()->default_value(0.5), "minimum k bin in units of fundamental frequency")
    ("dk", value<double>()->default_value(2.0), "k bin width in units of fundamental frequency")
    ("nbin-log", value<int>()->default_value(100), "number of logbinning; for --logbin")
    ("lognormal", value<bool>()->default_value(true), "set false for Gaussia delta")
    ("fix-amplitude", value<bool>()->default_value(false), "fix amplitude")
    ("rsd", value<bool>()->default_value(true), "anisotropic P(k, mu) with Kaiser")
    ("ofilename,o", value<string>()->default_value("mock.dat"), "output filename")
    //    ("compute-pk", value<string>(), "=filename; compute P(k) of delta_x grid")
    ("2D", "output 2D power spectrum")
    ("nmu", value<int>()->default_value(100), "number of mu bins")
    ("logbin", "logarithmic binning")
    ;
  
  positional_options_description p;
  p.add("filename", -1);
  
  variables_map vm;
  store(command_line_parser(argc, argv).options(opt).positional(p).run(), vm);
  notify(vm);

  if(vm.count("help") || ! vm.count("filename")) {
    cout << opt; 
    return 0;
  }

  const int nc= vm["nc"].as<int>(); assert(nc > 0);
  const double omega_m= vm["omega-m"].as<double>();
  const double z= vm["z"].as<double>();
  const double boxsize= vm["boxsize"].as<double>();
  const string filename= vm["filename"].as<string>();
  const bool rsd= vm["rsd"].as<bool>();
  const bool logbin = vm.count("logbin");

  nmu= vm["nmu"].as<int>(); assert(nmu > 0);

  // Load power spectrum

  // Set power spectrum amplitude
  cosmology_init(omega_m);
  const double a= 1.0/(1.0 + z); 
  const double growth= cosmology_D_growth(a);
  f= rsd ? cosmology_f_growth_rate(a) : 0.0;

  PowerSpectrum* const ps= power_alloc(filename.c_str(), growth);

  fprintf(stderr, "Growth_factor_D %.15e\n", growth);
  fprintf(stderr, "Growth_rate_f %.15e\n", f);

  if(vm.count("2D")) {
    write_power2mu(nc, boxsize, ps);
    return 0;
  }

  const int nckz= nc/2 + 1;

  const double fac= 2.0*M_PI/boxsize;
  const double knq= fac*nc/2;
  int nbin = 0;

  if(logbin) {
    k_min= 0.0;
    nbin = vm["nbin-log"].as<int>();
    dk = log10(static_cast<double>(nc/2))/nbin;
  }
  else {
    k_min= vm["k-min"].as<double>();
    dk= vm["dk"].as<double>();
    nbin= ceil(nc/2/dk);
  }
  
  vector<double> P0(nbin, 0.0);
  vector<double> P2(nbin, 0.0);
  vector<double> P4(nbin, 0.0);
  vector<int> nmodes(nbin, 0);
  
  for(int ix=0; ix<nc; ++ix) {
   int ikx= ix <= nc/2 ? ix : ix - nc;
   double kx= fac*ikx;
   
   for(int iy=0; iy<nc; ++iy) {
    int iky= iy <= nc/2 ? iy : iy - nc;
    double ky= fac*iky;

    int iz0 = !(kx > 0.0 || (kx == 0.0 && ky > 0.0));

    for(int iz=iz0; iz<nc/2; ++iz) {
      int ikz= iz;
      double kz= fac*ikz;

      double ik= sqrt(static_cast<double>(ikx*ikx + iky*iky + ikz*ikz));
      double k= sqrt(kx*kx + ky*ky + kz*kz);
      double mu = ikz/ik;
      double mu2= mu*mu;

      double fac= 1.0 + f*mu2;
      double P= fac*fac*power(ps, k);

      double l_2= 7.5*mu2 - 2.5;
      double l_4= 1.125*(35.0*mu2*mu2 - 30.0*mu2 + 3.0);

      int i=0;
      
      if(logbin)
	i= floor(log10(ik)/dk);
      else
	i= floor((ik - k_min)/dk);
      
      if(0 <= i && i < nbin) {
	nmodes[i]++;
	P0[i] += P;
	P2[i] += l_2*P;
	P4[i] += l_4*P;
      }
    }
   }
  }

  for(int i=0; i<nbin; ++i) {
    if(nmodes[i] > 0) {
      double k;
      if(logbin)
	k = pow(10.0, (i + 0.5)*dk);
      else
	k = (k_min + i + 0.5)*dk;

	
      printf("%.8e %.16e %.16e %.16e %d\n",
	     fac*k,
	     P0[i]/nmodes[i],
	     P2[i]/nmodes[i],
	     P4[i]/nmodes[i],
	     nmodes[i]);
    }
  }
  
  return 0;
}


void write_power2mu(const int nc, const double boxsize,
		    PowerSpectrum const * const ps)
{
  const int nckz= nc/2 + 1;

  const double fac= 2.0*M_PI/boxsize;
  const double knq= fac*nc/2;
  const int nbin= ceil(nc/2/dk);
  
  vector<double> P_mu(nbin*nmu, 0.0);
  vector<int> nmodes_mu(nbin*nmu, 0);
  vector<double> P_perp(nbin*nbin, 0.0);
  vector<int> nmodes_perp(nbin*nbin, 0);
  
  for(int ix=0; ix<nc; ++ix) {
   int ikx= ix <= nc/2 ? ix : ix - nc;
   double kx= fac*ikx;
   
   for(int iy=0; iy<nc; ++iy) {
    int iky= iy <= nc/2 ? iy : iy - nc;
    double ky= fac*iky;

    int iz0 = !(kx > 0.0 || (kx == 0.0 && ky > 0.0));

    for(int iz=iz0; iz<nc/2; ++iz) {
      int ikz= iz;
      double kz= fac*ikz;

      double ik= sqrt(static_cast<double>(ikx*ikx + iky*iky + ikz*ikz));
      double k= sqrt(kx*kx + ky*ky + kz*kz);
      //double mu= kz/k;
      double mu = ikz/ik;
      double mu2= mu*mu;

      double fac= 1.0 + f*mu2;
      double P= fac*fac*power(ps, k);

      int ikbin= floor((ik - k_min)/dk);
      int imubin = floor(mu*nmu);
      if(imubin == nmu) imubin--;

      // P(k, mu)
      if(0 <= ikbin && ikbin < nbin) {
	int i = ikbin*nmu + imubin;
	assert(0 <= i && i < nbin*nmu);
	nmodes_mu[i]++;
	P_mu[i] += P;
      }

      // P(k_perp, kz)
      double ik_perp= sqrt(static_cast<double>(ikx*ikx + iky*iky));
      int ik_perp_bin = floor((ik_perp - k_min)/dk);
      int ikz_bin = floor((ikz - k_min)/dk);
      if(0 <= ik_perp_bin && ik_perp_bin < nbin &&
	 0 <= ikz_bin && ikz_bin < nbin) {
	int i = ik_perp_bin*nbin + ikz_bin;
	assert(0 <= i && i < nbin*nbin);
	nmodes_perp[i]++;
	P_perp[i] += P;
      }
    }
   }
  }

  char filename[256];
  sprintf(filename, "pk_input_2D_polar_%d.txt", nc);
  FILE* fp= fopen(filename, "w");

  for(int i=0; i<nbin; ++i) {
    for(int j=0; j<nmu; ++j) {
      int index= i*nmu + j;
      if(nmodes_mu[index] > 0) {
	P_mu[index] /= nmodes_mu[index];
      }

      fprintf(fp, "%e %e %.16e %d\n",
	      fac*(k_min + i + 0.5)*dk,
	      (j + 0.5)/nmu,
	      P_mu[index], nmodes_mu[index]);
    }
  }

  // Column 1: k_z
  // Column 2: k_perp
  // Column 3: P(k_z, k_perp)
  // Column 4: nmodes
  fclose(fp);

  sprintf(filename, "pk_input_2D_cartesian_%d.txt", nc);
  fp= fopen(filename, "w");

  for(int i=0; i<nbin; ++i) {
    for(int j=0; j<nbin; ++j) {
      int index= i*nbin + j;
      if(nmodes_perp[index] > 0)
	P_perp[index] /= nmodes_perp[index];
      
      fprintf(fp, "%e %e %.16e %d\n",
	      fac*(k_min + i + 0.5)*dk,
	      fac*(k_min + j + 0.5)*dk,	      
	      P_perp[index], nmodes_perp[index]);
      
    }
  }

  fclose(fp);
}
