mock Lognormal w
================

Generate Gaussian or lognormal density field on a grid


Input
-----
Tabulated power spectrum extrapolated to z=0
k P(k)


Output
------

Column 1-3: comoving coodrinate (x1, x2, x3). x3 is the line of sight.
Column 4:   n(x, y, z)


Example
-------

Gaussian density fluctuation with fixed aplitude

mock_lognormal_w --nc=256 --boxsize=1000 --z=1 --seed=1 --lognormal=false --fix-amplitude=true -o mock_gaussian_fixed.dat planck_matterpower.dat


input power spectrum
====================

Computes expected input power spectrum; peform grid average and
multipole computation.

input_power_spectrum --nc=256 --boxsize=1000 --z=1 planck_matterpower.dat > pk_input.txt
